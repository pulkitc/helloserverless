const app = require('express')()
const axios = require('axios');
const chromium = require('chrome-aws-lambda');
const querystring = require('querystring');

app.get('/api', function (req, res) {
	const path = `/api/ipinfo`
	res.setHeader('Content-Type', 'text/html')
	res.setHeader('Cache-Control', 's-max-age=1, stale-while-revalidate')
	res.end(`Hello world! Go to ipinfo: <a href="${path}">${path}</a>`)
});

app.get('/api/ipinfo', function (req, res) {
	
	axios.get('http://ip-api.com/json/')
	.then(response => {
		res.status(200).json({data: response.data});
	})
	.catch(error => {
		console.log(error);
		res.status(500).json({'error': error});
	});
	
});

app.get('/api/pptr', async function (req, res) {
	
	let result = null;
	let browser = null;
	let gotourl = querystring.parse(req.url.split("?")[1]).url || 'https://www.primevideo.com/region/eu';
	
	try {
		browser = await chromium.puppeteer.launch({
			args: chromium.args,
			defaultViewport: chromium.defaultViewport,
			executablePath: await chromium.executablePath,
			headless: chromium.headless,
			ignoreHTTPSErrors: true,
		});
		
		let page = await browser.newPage();
		
		await page.goto(gotourl);
		
		result = await page.title();
		} catch (error) {
		console.log(error)
		if (browser !== null) {
			await browser.close();
		}
		return res.status(500).json({'error': JSON.stringify(error)});
		} finally {
		if (browser !== null) {
			await browser.close();
		}
	}
	res.status(200).json({data: result});	
});

module.exports = app

/*
	
	The correct way to only update package.json, without any other side-effects is:
	
	npm install --save --package-lock-only --no-package-lock <package>
	Use --package-lock-only to prevent writing to node_modules.
	
	The --package-lock-only argument will only update the package-lock.json, instead of checking node_modules and downloading dependencies.
	
	Then, use --no-package-lock to prevent the creation of the lock file:
	
	The --no-package-lock argument will prevent npm from creating a package-lock.json file. When running with package-lock's disabled npm will not automatically prune your node modules when installing.
	
	
*/